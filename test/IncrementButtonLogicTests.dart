
import 'package:flutter_app/incrementButtonScreenRedux/IncrementButtonLogic.dart';
import 'package:flutter_app/incrementButtonScreenRedux/IncrementButtonState.dart';
import 'package:test/test.dart';

void main() {
  test('test', () async {
    var initialState = IncrementButtonState(0, 18.0);
    var counter = await incrementCounter(initialState.counter, initialState.counterSize);
    expect(counter.counterValue, 1);
    expect(counter.counterSize, 20.0);
  });
}
