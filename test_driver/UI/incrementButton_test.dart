import 'package:flutter_driver/flutter_driver.dart';
import 'package:test/test.dart';

// run test from root of project: flutter drive --target=test_driver/UI/incrementButton.dart
void main() {

  group("increment button test", () {
    FlutterDriver driver;

    setUpAll(() async {
      driver = await FlutterDriver.connect();
    });

    tearDownAll(() async {
      driver?.close();
    });

    test("tester",  () async{

      await driver.waitFor(find.text("0"));
      await driver.tap(find.byTooltip("Increment")); // searching by key is currently not working in flutter integration tests, its kinda buggy,
      await driver.waitFor(find.text("1"));

    });
  });
}
